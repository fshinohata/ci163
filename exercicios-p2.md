CI163 - Exercícios para P2 (2019/1)
===================================

**1) O que é um padrão de software?**

Um padrão de software é um conjunto de "princípios" de desenvolvimento de software.

Um padrão surge como solução de um problema ou conjunto de problemas, logo a escolha de um único padrão nem sempre é a saída, especialmente para projetos de maior porte. Portanto, "padrão de software" nesse sentido é um meio para lidar com o problema que ele se propõe a resolver.

**2) O padrão *Information Expert* define princípios para a atribuição de responsabilidades aos objetos.**

**a) qual é o princípio central do padrão?**

É a definição de responsabilidades. Um objeto ou classe deve ter seu domínio bem definido, e seus métodos devem estar estritamente relacionados ao domínio. Por exemplo, uma classe/objeto `Calculadora` não deve realizar ações não relacionadas ao seu domínio, como concatenar *strings*.

**b) que benefícios tenho ao aplicar o padrão?**

O padrão *Information Expert* garante alta coesão e baixo acoplamento, uma vez que cada "*Expert*" lida com um domínio específico.

**3) Considere as classes `Aluno`, `Turma` e `Matricula`, em um sistema de gerenciamento acadêmico. Uma `Turma` poderá ter várias `Matriculas`, cada uma referente a um único `Aluno`. Um `Aluno` poderá ter várias matrículas. Uma `Matricula` armazenará pelo menos a nota do `Aluno`. O sistema poderá calcular a nota de cada aluno. Defina o diagrama UML deste sistema, e explique como podera usar o padrão *Information Expert* para especificar o método de cálculo da nota.**

Classes:

```none
class Turma {
    public pesoProvas: float[]
    private matriculas: Matricula[]
    public calculaMediaTurma(): float
}

class Matricula {
    public notas: float[]
    private turma: Turma
    private aluno: Aluno
    public calculaMedia(): float
}

class Aluno {
    private matriculas: Matricula[]
    public getNotaFinal(Turma t): float
}
```

Relações:

```none
Matricula is part of Turma (strong composition) (1x1)
Matricula is part of Aluno (strong composition) (1x1)
Turma has many Matricula (1xN)
Aluno has many Matricula (1xN)
Aluno has many Turma through Matricula (NxN)
```

Aplicando o padrão *Information Expert*:

* a `Matricula` é o "*Expert*" que conhece as notas do aluno na turma;

* a `Turma` é o "*Expert*" que conhece o peso de cada nota (provas e trabalhos);

Logo, a `Matricula` é o "*Expert*" mais adequado para calcular a média do aluno na turma, pois o dado principal (notas do aluno) está contido nela, e é ela que tem uma relação direta com `Aluno`.

**4) Considere um sistema de gestão de bibliotecas, contendo as classes `Livro`, `Usuario` e `Emprestimo`. Quem deveria ser o responvável pelo cálculo de multa de devolução? E pela devolução do livro? Explique o padrão seguido.**

O cálcula de multa de devolução depende da data de empréstimo e da "duração" do empréstimo. Logo, a entidade `Emprestimo` seria a mais adequada para o cálculo sob o padrão *Information Expert*.

A devolução de um livro é um ato, uma ação relacionada diretamente ao `Usuario`. Portanto, o `Usuario` é a entidade mais adequada para realizar/iniciar esta ação.

Em ambos os casos o padrão *Information Expert* foi escolhido, por ser um padrão "natural", intuitivo, especialmente em casos orientados à objetos.

**5) No caso do sistema acadêmico apresentado no exercício (3), quem deve ser o responsável pela criação das matrículas? Explique o padrão utilizado na escolha.**

Considerando o caso da UFPR onde os alunos escolhem as matérias que irão cursar em cada semestre, a entidade `Aluno` é a mais adequada para criar as instâncias de `Matricula`.

Essa lógica segue o padrão `Creator`, que determina responsabilidades de instanciação de entidades. O padrão diz que o responsável por instanciar uma classe é a classe "mais relacionada", cujas ações são mais próximas. Estamos acostumados a dizer que um aluno tem matrículas, e não que uma turma tem matrículas. Além disso, conforme dito no início da resposta, no caso da UFPR os alunos é quem se matriculam nas disciplinas.

**6) Porque devemos priorizar "alta coesão" e "baixo acoplamento" na aplicação de um padrão GRASP?**

Os padrões GRASP por natureza tentam manter alta coesão e/ou baixo acoplamento. Por exemplo, o *Information Expert* é um padrão que foca na independência e na boa definição de responsabilidades, que resultam em baixo acoplamento e alta coesão, respectivamente.

O padrão *Controller* utiliza entidades intermediárias que delegam responsabilidades para classes terceiras, resultando em um baixo acoplamento entre essas classes terceiras.

O padrão *Pure Fabrication* utiliza entidades cujo papel é simplesmente instanciar/salvar classes terceiras, resultando em uma alta coesão para essas classes terceiras, pois a lógica de instanciar/salvar no meio do código atrapalha e diminui a coesão.

Em outras palavras, dado que você sabe qual padrão foi aplicado, todos eles buscam alta coesão e/ou baixo acoplamento, no fim das contas.

**7) O que é acoplamento de dados? Cite um exemplo abstrato.**

Acoplamento de dados é a interdependência de dados entre classes. Muitas vezes ocorre de uma classe precisar de dados que estão em outras classes para cumprir sua tarefa, e essa necessidade em si é o acoplamento de dados.

**8) Cite um exemplo de um acoplamento de dado global.**

Uma classe *Singleton* chamada `Environment`, que contém todas as informações sobre o ambiente em que o sistema está rodando. Se em algum momento o código altera esse ambiente, todo o sistema irá reagir à essa mudança de alguma forma. Um exemplo clássico é diferenciar se o ambiente é "*Development*" ou "*Production*", pois em "*Development*" geralmente vários *logs* e funcionalidades de depuração são ativados.

**9) Que tipo de objeto geralmente deve ser responsável por receber eventos do sistema? Isto é definido em qual padrão?**

Objetos que tem a responsabilidade de analisar qual é o evento e então delegar a tarefa para a entidade mais adequada.

Esse tipo de objeto pode ser visto como um *Controller* (padrão GRASP) ou como um *Observer* (padrão GOF).

**10) Por que não é adequado um objeto *Controller* realizar ações do domínio da aplicação?**

Simplesmente porque o papel de um *Controller* é analisar a requisição e delegar a atividade para a entidade mais adequada. Portanto, se um *Controller* realizar ações do domínio da aplicação, ele estará "fugindo" de seu papel principal, e isso resulta em baixa coesão e um acoplamento desnecessário entre *Controller* e aplicação.

**11) Explique um exemplo de solução, usando o padrão *Pure Fabrication*, para armazenamento de dados em um repositório. Por que não usar estritamente o padrão *Information Expert*?**

Considere a seguinte solução que usa o padrão *Pure Fabrication*:

```csharp
public static class UserSql {
    public static IEnumerable<User> QueryAll() {
        // SQL que busca e retorna todos os usuarios do banco
    }
    public static void Save(User user) {
        // SQL que salva usuario no banco
    }
}

public class User {
    public long ID;
    public string FirstName;
    public string LastName;
    public string GetFullName() {
        return FirstName + " " + LastName;
    }
}

public class System {
    public IEnumerable<User> GetUsers() {
        return UserSql.QueryAll();
    }
}
```

Nessa solução, a classe `User` é uma entidade bem definida, que realiza ações relacionadas ao que ela representa.

Essa mesma solução, utilizando o padrão *Information Expert*, seria algo assim:

```csharp
public class User {
    public long ID;
    public string FirstName;
    public string LastName;
    public string GetFullName() {
        return FirstName + " " + LastName;
    }
    public static IEnumerable<User> QueryAll() {
        // SQL que busca e retorna todos os usuarios do banco
    }
    public static void Save(User user) {
        // SQL que salva usuario no banco
    }
}

public class System {
    public IEnumerable<User> GetUsers() {
        return User.QueryAll();
    }
}
```

Repare que não ficou muito diferente: os métodos da classe `UserSql` (que era a entidade *Pure Fabrication*) foram movido para a classe `User`. O problema disso é que agora a classe `User` possui métodos não relacionadas a ela, pois salvar ou pegar instâncias do banco de dados não é uma coisa do domínio `User`.

Esse tipo de ação existe para todas as entidades que precisam ser salvar no banco de dados. Ou seja, não são ações relacionadas às entidades, mas ao banco de dados. Portanto, utilizar o padrão *Information Expert* estritamente pode resultar em baixa coesão nesses casos, uma vez que as classes sempre teriam ações não relacionadas a elas incorporadas no meio.

**12) Defina o código orientado a objeto para o exercício (3)**

```csharp
// C#
public class Turma
{
    public IEnumerable<decimal> PesoProvas;
    private IEnumerable<Matricula> _matriculas;
    public decimal CalculaMediaTurma()
    {
        // calcula media usando {_matriculas}
    }
}

public class Matricula
{
    public IEnumerable<decimal> Notas;
    private Turma _turma;
    private Aluno _aluno;
    public decimal CalculaMedia()
    {
        // calcula media usando {_turma.PesoProvas} e {Notas}
    }
}

public class Aluno
{
    private IEnumerable<Matricula> _matriculas;
}
```

**13) O formato JSON (JavaScript Object Notation) pode ser usado como formato para representação de modelos. O formato define documentos, que possuem atributos, que podem ter valores inteiros, string e outros objetos, e arrays, que possuem listas de objetos. É um formato mais restrito que UML, porém é simples de implementar e utilizar. Um exemplo de JSON está abaixo:**

```json
{
    "aluno": "nome do aluno",
    "endereco": {
        "rua": "rua xv",
        "numero": 12
    },
    "telefone": {
        "residencial": "123456",
        "comercial": "654321"
    }
}
```

**Defina o código em JSON para o exercício (3).**

```json
{
    "turma": {
        "pesoProvas": [
            1,
            1,
            2
        ]
    },
    "matricula": {
        "notas": [
            7,
            7.5,
            8
        ],
        "turma": {
            "pesoProvas": [
                1,
                1,
                2
            ]
        },
        "aluno": {}
    },
    "aluno": {}
}
```

**14) Explique a vantagem em usar um gerador de código para criar código objeto a partir de um diagrama UML.**

Uma vantagem imediata é que o diagrama UML ganha uma utilidade a mais, que é gerar o código *boilerplate* do projeto após sua definição estar concluída.

Essa possibilidade diminui drasticamente as chances do modelo ficar muito diferente da implementação, e salva trabalho dos desenvolvedores de precisar escrever o "formato" das classes (nome, métodos, propriedades, etc).

**15) Dado o JSON do exercício 13, quais são os elementos específicos da linguagem e quais são os elementos variáveis?**

Os elementos específicos da linguagem são as regras de formatação: objetos são enclausulados por chaves `{}`, o nome das propriedades são obrigatoriamente strings `{ "prop": value }` e os valores devem ser números, strings, arrays `[]` ou objetos `{}`.

Tirando essa formalidade, os valores das propriedades e todo o restante dos dados são variáveis. Nome de propriedades, valores, nível de aninhamento, tudo pode ser definido livremente pelo usuário, bastando seguir as regras de sintaxe.

**16) Defina, sob a forma de um exemplo, uma nova linguagem textual para projetar um diagrama de classes UML. A linguagem deverá ter no mínimo, classes, atributos e associações.**

