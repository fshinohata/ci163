import { Entity, model, property } from '@loopback/repository';

@model({ settings: {} })
export class Todo extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @property({
    type: 'boolean',
    required: true,
    default: false,
  })
  isComplete: boolean;

  @property({
    type: 'number',
    required: false,
  })
  todoListId: number;


  constructor(data?: Partial<Todo>) {
    super(data);
  }
}
