import { Entity, model, property, hasMany } from '@loopback/repository';
import { Todo } from './todo.model';

@model({ settings: {} })
export class TodoList extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
  })
  description?: string;

  @hasMany(() => Todo)
  todos?: Todo[];


  constructor(data?: Partial<TodoList>) {
    super(data);
  }
}
