import { repository, Filter } from "@loopback/repository";
import { TodoListRepository } from "../repositories";
import { post, param, requestBody, get } from "@loopback/rest";
import { Todo } from "../models";

// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/context';

export class TodoListTodoController {
  constructor(
    @repository(TodoListRepository) protected todoListRepository: TodoListRepository
  ) { }

  @post('/todo-lists/{id}/todo')
  async create(@param.path.number('id') id: number, @requestBody() todo: Todo): Promise<Todo> {
    todo.todoListId = id;
    return await this.todoListRepository.todos(id).create(todo);
  }

  @get('todo-lists/{id}/todos')
  async find(@param.path.number('id') id: number, @param.query.object('filter') filter?: Filter): Promise<Todo[]> {
    return await this.todoListRepository.todos(id).find(filter);
  }
}
