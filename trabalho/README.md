Trabalho de Projeto de Software (2019/1)
========================================

**Recomenda-se ler a versão exportada para HTML (README.html) em um browser.**

Equipe:

* Fernando Aoyagui Shinohata - GRR20165388

Sobre a elaboração do diagrama UML
----------------------------------

O diagrama de classes UML foi montado com a ferramenta [GenMyModel](https://www.genmymodel.com/), que suporta a exportação do diagrama para o formato `.xmi` e para imagem. O problema é que a *feature* de exportação para imagem é paga, então, para fins deste trabalho, as imagens apresentadas aqui são *screenshots* recortados, visto que o modelo é bem pequeno, e por isso "cabia" na tela.

Junto a este arquivo estão duas versões do diagrama:

* `diagrama-original.png`: é a primeira versão, antes de passar para a fase de implementação no LoopBack;

* `diagrama-final.png`: é a versão adaptada para suportar os requerimentos da *framework*. Como os métodos não são dos modelos na *framework* (são dos *controllers*/*repositories*), todas as operações da versão original foram removidas.

**Obs:** O arquivo `diagramas.xmi` é o arquivo-fonte gerado pelo [GenMyModel](https://www.genmymodel.com/).

Nas imagens:

* Enumerações e tipos de dados estão em roxo claro;

* As classes estão em azul claro;

* Os métodos foram retirados das classes no diagrama final por que os *models* no Loopback não contém métodos (eles foram movidos para os *controllers* e *repositories*). Como o diagrama representa as entidades do sistema, foi decidido que apenas os *models* fossem representados, ao invés de uma mistura de coisas;

* Os tipos de dados (*datatypes*) só existem porque a ferramenta não permitia declarar um tipo `Playlist[]` ou `Music[]`. O correto seria explicitar que existe uma dependência entre esse tipo de dado e as classes que a usam (com a flecha pontilhada), mas o diagrama ficaria desnecessariamente poluído;

    * O tipo de dado *void* só foi criado para explicitar que nada é retornado, já que a ferramenta deixava vazio;

* As visibilidades de propriedades/métodos utilizadas são `private` (**-**) e `public` (**+**) no diagrama original, e apenas `public` no diagrama final;

    * Os repositórios do Loopback não criam instâncias dos *models* corretamente quando propriedades `private` ou `readonly` são usadas, forçando a remoção dessas propriedades.

Sobre a elaboração do projeto em LoopBack
-----------------------------------------

A maior dificuldade foi encontrar uma boa documentação sobre como implementar relações.

O Loopback 3 possui suporte nativo para relações NxN usando tabelas "pivô" (intermediárias) através da relação `HasManyThrough`, mas o Loopback 4 não. Essa relação foi montada através de duas relações `HasMany`:

```
A HasMany PIVOT
B HasMany PIVOT
PIVOT belongs to A // (implementação opcional)
PIVOT belongs to B // (implementação opcional)
```

Outro problema foi a implementação das enumerações presentes no `diagrama-original.png`. Aparentemente, o Loopback 4 não suporta ainda o tipo `enum` do TypeScript, então enumerações devem ser criadas através de validações (*i.e.* validar que o campo X só pode assumir os valores A, B, C) ao invés de um suporte nativo.

Essas validações **não foram implementadas** neste trabalho. Logo, os campos são simplesmente do tipo *string*.

Em geral, quase todos os métodos foram gerados pela *framework*, sem necessidade de alteração.

Apenas os seguintes adicionais foram colocados:

* Na criação de um `User`, é criado também seu `MusicDirectory`;

* *Endpoint* que retorna a lista de *playlists* compartilhadas (`/music-directories/{id}/shared-playlists`);

* *Endpoint* que retorna a lista de músicas de uma `Playlist` (`/playlists/{id}/musics`);

* *Endpoint* que retorna a lista de músicas de um `Album` (`/albums/{id}/musics`).

Todos os demais *endpoints* foram gerados automagicamente por algum dos comandos da CLI.

### Sobre os *endpoints*

Uma boa parte dos *endpoints* foram retirados do projeto por falta de coesão.

Por exemplo, os métodos **create**, **patch** e **delete** do *controller* `PlaylistController` foram retirados, uma vez que esses métodos já existem [e só fazem sentido] no *controller* da relação `MusicDirectory->Playlist`.

Banco de Dados
--------------

Este é o banco de dados pré-preenchido (simplificado):

```none
Albums: {
    1: {
        name: Album 1
        style: Rock,
        musics: [1, 2],
    },
    2: {
        name: Album 2
        style: Pop,
        musics: [3, 4],
    },
},
Musics: {
    1: {
        name: Music 1,
        style: Rock,
        albums: [1],
    },
    2: {
        name: Music 2,
        style: Rock,
        albums: [1],
    },
    3: {
        name: Music 3,
        style: Pop,
        albums: [2],
    },
    4: {
        name: Music 4,
        style: Pop,
        albums: [2],
    },
},
Users: {
    1: {
        login: Fernando,
        musicDirectoryId: 1,
    },
    2: {
        login: Shinohata,
        musicDirectoryId: 2,
    }
},
MusicDirectories: {
    1: {
        playlists: [1],
        sharedPlaylists: [2],
    },
    2: {
        playlists: [2],
        sharedPlaylists: [],
    },
},
Playlists: {
    1: {
        name: First of Each Album,
        musics: [1, 3],
        musicDirectoryId: 1,
    },
    2: {
        name: Second of Each Album,
        musics: [2, 4],
        musicDirectoryId: 2,
    },
},
```

Compartilhamento de Playlists
-----------------------------

O compartilhamento entre usuários é feito, no banco de dados, com a dupla `(musicDirectoryId, playlistId)`, ao invés de `(userId, playlistId)`. Essa decisão foi tomada porque o diretório de músicas deve conter as *playlists* do usuário, então não faria sentido que o compartilhamento fosse com o usuário ao invés do diretório.

O compartilhamento é feito através dos seguintes *endpoints*:

```none
(GET) /playlists/{id}/shared-playlists
(POST) /playlists/{id}/shared-playlists
(PATCH) /playlists/{id}/shared-playlists
(DELETE) /playlists/{id}/shared-playlists
```

Todos estes *endpoints* servem para ver/criar/atualizar/deletar instâncias de `SharedPlaylist`, que é um objeto-dupla (`musicDirectoryId`, `playlistId`).

Em outras palavras, uma instância `<SharedPlaylist>(x, y)` diz que a *playlist* `y` está compartilhada com o diretório de músicas `x`.

Para confirmar o compartilhamento você pode verificar as *playlists* compartilhadas com o diretório de músicas `x` através do *endpoint* `"/music-directories/{id}/shared-playlists"`.
