import {DefaultCrudRepository} from '@loopback/repository';
import {SharedPlaylist, SharedPlaylistRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class SharedPlaylistRepository extends DefaultCrudRepository<
  SharedPlaylist,
  typeof SharedPlaylist.prototype.id,
  SharedPlaylistRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(SharedPlaylist, dataSource);
  }
}
