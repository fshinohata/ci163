import {DefaultCrudRepository} from '@loopback/repository';
import {PlaylistMusic, PlaylistMusicRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class PlaylistMusicRepository extends DefaultCrudRepository<
  PlaylistMusic,
  typeof PlaylistMusic.prototype.id,
  PlaylistMusicRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(PlaylistMusic, dataSource);
  }
}
