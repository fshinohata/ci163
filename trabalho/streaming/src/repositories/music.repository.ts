import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Music, MusicRelations, AlbumMusic, PlaylistMusic} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {AlbumMusicRepository} from './album-music.repository';
import {PlaylistMusicRepository} from './playlist-music.repository';

export class MusicRepository extends DefaultCrudRepository<
  Music,
  typeof Music.prototype.id,
  MusicRelations
> {

  public readonly albumMusics: HasManyRepositoryFactory<AlbumMusic, typeof Music.prototype.id>;

  public readonly playlistMusics: HasManyRepositoryFactory<PlaylistMusic, typeof Music.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('AlbumMusicRepository') protected albumMusicRepositoryGetter: Getter<AlbumMusicRepository>, @repository.getter('PlaylistMusicRepository') protected playlistMusicRepositoryGetter: Getter<PlaylistMusicRepository>,
  ) {
    super(Music, dataSource);
    this.playlistMusics = this.createHasManyRepositoryFactoryFor('playlistMusics', playlistMusicRepositoryGetter,);
    this.albumMusics = this.createHasManyRepositoryFactoryFor('albumMusics', albumMusicRepositoryGetter,);
  }
}
