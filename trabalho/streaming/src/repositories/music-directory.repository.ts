import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {MusicDirectory, MusicDirectoryRelations, Playlist, SharedPlaylist} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {PlaylistRepository} from './playlist.repository';
import {SharedPlaylistRepository} from './shared-playlist.repository';

export class MusicDirectoryRepository extends DefaultCrudRepository<
  MusicDirectory,
  typeof MusicDirectory.prototype.id,
  MusicDirectoryRelations
> {

  public readonly playlists: HasManyRepositoryFactory<Playlist, typeof MusicDirectory.prototype.id>;

  public readonly sharedPlaylists: HasManyRepositoryFactory<SharedPlaylist, typeof MusicDirectory.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('PlaylistRepository') protected playlistRepositoryGetter: Getter<PlaylistRepository>, @repository.getter('SharedPlaylistRepository') protected sharedPlaylistRepositoryGetter: Getter<SharedPlaylistRepository>,
  ) {
    super(MusicDirectory, dataSource);
    this.sharedPlaylists = this.createHasManyRepositoryFactoryFor('sharedPlaylists', sharedPlaylistRepositoryGetter,);
    this.playlists = this.createHasManyRepositoryFactoryFor('playlists', playlistRepositoryGetter,);
  }
}
