import {DefaultCrudRepository} from '@loopback/repository';
import {AlbumMusic, AlbumMusicRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AlbumMusicRepository extends DefaultCrudRepository<
  AlbumMusic,
  typeof AlbumMusic.prototype.id,
  AlbumMusicRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(AlbumMusic, dataSource);
  }
}
