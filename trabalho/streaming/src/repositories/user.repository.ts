import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {User, UserRelations, MusicDirectory} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {MusicDirectoryRepository} from './music-directory.repository';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {

  public readonly musicDirectory: BelongsToAccessor<MusicDirectory, typeof User.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('MusicDirectoryRepository') protected musicDirectoryRepositoryGetter: Getter<MusicDirectoryRepository>,
  ) {
    super(User, dataSource);
    this.musicDirectory = this.createBelongsToAccessorFor('musicDirectory', musicDirectoryRepositoryGetter,);
  }
}
