import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Album, AlbumRelations, AlbumMusic} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {AlbumMusicRepository} from './album-music.repository';

export class AlbumRepository extends DefaultCrudRepository<
  Album,
  typeof Album.prototype.id,
  AlbumRelations
> {

  public readonly albumMusics: HasManyRepositoryFactory<AlbumMusic, typeof Album.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('AlbumMusicRepository') protected albumMusicRepositoryGetter: Getter<AlbumMusicRepository>,
  ) {
    super(Album, dataSource);
    this.albumMusics = this.createHasManyRepositoryFactoryFor('albumMusics', albumMusicRepositoryGetter,);
  }
}
