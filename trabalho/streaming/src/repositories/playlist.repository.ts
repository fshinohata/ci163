import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Playlist, PlaylistRelations, PlaylistMusic, SharedPlaylist} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {PlaylistMusicRepository} from './playlist-music.repository';
import {SharedPlaylistRepository} from './shared-playlist.repository';

export class PlaylistRepository extends DefaultCrudRepository<
  Playlist,
  typeof Playlist.prototype.id,
  PlaylistRelations
> {

  public readonly playlistMusics: HasManyRepositoryFactory<PlaylistMusic, typeof Playlist.prototype.id>;

  public readonly sharedPlaylists: HasManyRepositoryFactory<SharedPlaylist, typeof Playlist.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('PlaylistMusicRepository') protected playlistMusicRepositoryGetter: Getter<PlaylistMusicRepository>, @repository.getter('SharedPlaylistRepository') protected sharedPlaylistRepositoryGetter: Getter<SharedPlaylistRepository>,
  ) {
    super(Playlist, dataSource);
    this.sharedPlaylists = this.createHasManyRepositoryFactoryFor('sharedPlaylists', sharedPlaylistRepositoryGetter,);
    this.playlistMusics = this.createHasManyRepositoryFactoryFor('playlistMusics', playlistMusicRepositoryGetter,);
  }
}
