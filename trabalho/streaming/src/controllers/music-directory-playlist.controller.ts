import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  MusicDirectory,
  Playlist,
} from '../models';
import { MusicDirectoryRepository, PlaylistRepository } from '../repositories';

export class MusicDirectoryPlaylistController {
  constructor(
    @repository(MusicDirectoryRepository) protected musicDirectoryRepository: MusicDirectoryRepository,
    @repository(PlaylistRepository) protected playlistRepository: PlaylistRepository,
  ) { }

  @get('/music-directories/{id}/shared-playlists')
  async findShared(
    @param.path.number('id') id: number,
  ): Promise<Playlist[]> {
    return await this.musicDirectoryRepository.sharedPlaylists(id).find()
      .then(async sharedPlaylists =>
        await this.playlistRepository.find({
          where: {
            or: sharedPlaylists.map(sharedPlaylist => ({ id: sharedPlaylist.playlistId }))
          }
        })
      );
  }

  @get('/music-directories/{id}/playlists', {
    responses: {
      '200': {
        description: 'Array of Playlist\'s belonging to MusicDirectory',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Playlist } },
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Playlist>,
  ): Promise<Playlist[]> {
    return await this.musicDirectoryRepository.playlists(id).find(filter);
  }

  @post('/music-directories/{id}/playlists', {
    responses: {
      '200': {
        description: 'MusicDirectory model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Playlist } } },
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof MusicDirectory.prototype.id,
    @requestBody() playlist: Playlist,
  ): Promise<Playlist> {
    playlist.musicDirectoryId = id;
    return await this.musicDirectoryRepository.playlists(id).create(playlist);
  }

  @patch('/music-directories/{id}/playlists', {
    responses: {
      '200': {
        description: 'MusicDirectory.Playlist PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody() playlist: Partial<Playlist>,
    @param.query.object('where', getWhereSchemaFor(Playlist)) where?: Where<Playlist>,
  ): Promise<Count> {
    playlist.musicDirectoryId = id;
    return await this.musicDirectoryRepository.playlists(id).patch(playlist, where);
  }

  @del('/music-directories/{id}/playlists', {
    responses: {
      '200': {
        description: 'MusicDirectory.Playlist DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Playlist)) where?: Where<Playlist>,
  ): Promise<Count> {
    return await this.musicDirectoryRepository.playlists(id).delete(where);
  }
}
