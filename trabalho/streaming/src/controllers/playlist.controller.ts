import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
} from '@loopback/rest';
import { Playlist } from '../models';
import { PlaylistRepository } from '../repositories';

export class PlaylistController {
  constructor(
    @repository(PlaylistRepository)
    public playlistRepository: PlaylistRepository,
  ) { }

  @get('/playlists/count', {
    responses: {
      '200': {
        description: 'Playlist model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Playlist)) where?: Where<Playlist>,
  ): Promise<Count> {
    return await this.playlistRepository.count(where);
  }

  @get('/playlists', {
    responses: {
      '200': {
        description: 'Array of Playlist model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Playlist } },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Playlist)) filter?: Filter<Playlist>,
  ): Promise<Playlist[]> {
    return await this.playlistRepository.find(filter);
  }

  @get('/playlists/{id}', {
    responses: {
      '200': {
        description: 'Playlist model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Playlist } } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Playlist> {
    return await this.playlistRepository.findById(id);
  }
}
