import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Album,
  AlbumMusic,
  Music,
} from '../models';
import { AlbumRepository, MusicRepository } from '../repositories';

export class AlbumAlbumMusicController {
  constructor(
    @repository(AlbumRepository) protected albumRepository: AlbumRepository,
    @repository(MusicRepository) protected musicRepository: MusicRepository,
  ) { }

  @get('/albums/{id}/musics')
  async findMusics(
    @param.path.number('id') id: number,
  ): Promise<Music[]> {
    return await this.albumRepository.albumMusics(id).find()
      .then(async albumMusics =>
        await this.musicRepository.find({
          where: {
            or: albumMusics.map(albumMusic => ({
              id: albumMusic.musicId
            }))
          }
        })
      );
  }

  @get('/albums/{id}/album-musics', {
    responses: {
      '200': {
        description: 'Array of AlbumMusic\'s belonging to Album',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': AlbumMusic } },
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<AlbumMusic>,
  ): Promise<AlbumMusic[]> {
    return await this.albumRepository.albumMusics(id).find(filter);
  }

  @post('/albums/{id}/album-musics', {
    responses: {
      '200': {
        description: 'Album model instance',
        content: { 'application/json': { schema: { 'x-ts-type': AlbumMusic } } },
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Album.prototype.id,
    @requestBody() albumMusic: AlbumMusic,
  ): Promise<AlbumMusic> {
    albumMusic.albumId = id;
    return await this.albumRepository.albumMusics(id).create(albumMusic);
  }

  @patch('/albums/{id}/album-musics', {
    responses: {
      '200': {
        description: 'Album.AlbumMusic PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody() albumMusic: Partial<AlbumMusic>,
    @param.query.object('where', getWhereSchemaFor(AlbumMusic)) where?: Where<AlbumMusic>,
  ): Promise<Count> {
    albumMusic.albumId = id;
    return await this.albumRepository.albumMusics(id).patch(albumMusic, where);
  }

  @del('/albums/{id}/album-musics', {
    responses: {
      '200': {
        description: 'Album.AlbumMusic DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(AlbumMusic)) where?: Where<AlbumMusic>,
  ): Promise<Count> {
    return await this.albumRepository.albumMusics(id).delete(where);
  }
}
