import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
} from '@loopback/rest';
import {
  User,
  MusicDirectory,
} from '../models';
import {UserRepository} from '../repositories';

export class UserMusicDirectoryController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @get('/users/{id}/music-directory', {
    responses: {
      '200': {
        description: 'MusicDirectory belonging to User',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': MusicDirectory } },
          },
        },
      },
    },
  })
  async getMusicDirectory(
    @param.path.number('id') id: typeof User.prototype.id,
  ): Promise<MusicDirectory> {
    return await this.userRepository.musicDirectory(id);
  }
}
