import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Playlist,
  PlaylistMusic,
  Music,
} from '../models';
import { PlaylistRepository, MusicRepository } from '../repositories';

export class PlaylistPlaylistMusicController {
  constructor(
    @repository(PlaylistRepository) protected playlistRepository: PlaylistRepository,
    @repository(MusicRepository) protected musicRepository: MusicRepository,
  ) { }

  @get('/playlists/{id}/musics')
  async findMusics(
    @param.path.number('id') id: number,
  ): Promise<Music[]> {
    return await this.playlistRepository.playlistMusics(id).find()
      .then(async playlistMusics =>
        await this.musicRepository.find({
          where: {
            or: playlistMusics.map(playlistMusic => ({ id: playlistMusic.musicId }))
          }
        })
      )
  }

  @get('/playlists/{id}/playlist-musics', {
    responses: {
      '200': {
        description: 'Array of PlaylistMusic\'s belonging to Playlist',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': PlaylistMusic } },
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<PlaylistMusic>,
  ): Promise<PlaylistMusic[]> {
    return await this.playlistRepository.playlistMusics(id).find(filter);
  }

  @post('/playlists/{id}/playlist-musics', {
    responses: {
      '200': {
        description: 'Playlist model instance',
        content: { 'application/json': { schema: { 'x-ts-type': PlaylistMusic } } },
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Playlist.prototype.id,
    @requestBody() playlistMusic: PlaylistMusic,
  ): Promise<PlaylistMusic> {
    playlistMusic.playlistId = id;
    return await this.playlistRepository.playlistMusics(id).create(playlistMusic);
  }

  @patch('/playlists/{id}/playlist-musics', {
    responses: {
      '200': {
        description: 'Playlist.PlaylistMusic PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody() playlistMusic: Partial<PlaylistMusic>,
    @param.query.object('where', getWhereSchemaFor(PlaylistMusic)) where?: Where<PlaylistMusic>,
  ): Promise<Count> {
    playlistMusic.playlistId = id;
    return await this.playlistRepository.playlistMusics(id).patch(playlistMusic, where);
  }

  @del('/playlists/{id}/playlist-musics', {
    responses: {
      '200': {
        description: 'Playlist.PlaylistMusic DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(PlaylistMusic)) where?: Where<PlaylistMusic>,
  ): Promise<Count> {
    return await this.playlistRepository.playlistMusics(id).delete(where);
  }
}
