import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
} from '@loopback/rest';
import { MusicDirectory } from '../models';
import { MusicDirectoryRepository } from '../repositories';

export class MusicDirectoryController {
  constructor(
    @repository(MusicDirectoryRepository)
    public musicDirectoryRepository: MusicDirectoryRepository,
  ) { }

  @get('/music-directories/count', {
    responses: {
      '200': {
        description: 'MusicDirectory model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(MusicDirectory)) where?: Where<MusicDirectory>,
  ): Promise<Count> {
    return await this.musicDirectoryRepository.count(where);
  }

  @get('/music-directories', {
    responses: {
      '200': {
        description: 'Array of MusicDirectory model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': MusicDirectory } },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(MusicDirectory)) filter?: Filter<MusicDirectory>,
  ): Promise<MusicDirectory[]> {
    return await this.musicDirectoryRepository.find(filter);
  }

  @get('/music-directories/{id}', {
    responses: {
      '200': {
        description: 'MusicDirectory model instance',
        content: { 'application/json': { schema: { 'x-ts-type': MusicDirectory } } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<MusicDirectory> {
    return await this.musicDirectoryRepository.findById(id);
  }
}
