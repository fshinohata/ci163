import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Playlist,
  SharedPlaylist,
} from '../models';
import { PlaylistRepository } from '../repositories';

export class PlaylistSharedPlaylistController {
  constructor(
    @repository(PlaylistRepository) protected playlistRepository: PlaylistRepository,
  ) { }

  @get('/playlists/{id}/shared-playlists', {
    responses: {
      '200': {
        description: 'Array of SharedPlaylist\'s belonging to Playlist',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': SharedPlaylist } },
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<SharedPlaylist>,
  ): Promise<SharedPlaylist[]> {
    return await this.playlistRepository.sharedPlaylists(id).find(filter);
  }

  @post('/playlists/{id}/shared-playlists', {
    responses: {
      '200': {
        description: 'Playlist model instance',
        content: { 'application/json': { schema: { 'x-ts-type': SharedPlaylist } } },
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Playlist.prototype.id,
    @requestBody() sharedPlaylist: SharedPlaylist,
  ): Promise<SharedPlaylist> {
    sharedPlaylist.playlistId = id;
    return await this.playlistRepository.sharedPlaylists(id).create(sharedPlaylist);
  }

  @patch('/playlists/{id}/shared-playlists', {
    responses: {
      '200': {
        description: 'Playlist.SharedPlaylist PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody() sharedPlaylist: Partial<SharedPlaylist>,
    @param.query.object('where', getWhereSchemaFor(SharedPlaylist)) where?: Where<SharedPlaylist>,
  ): Promise<Count> {
    sharedPlaylist.playlistId = id;
    return await this.playlistRepository.sharedPlaylists(id).patch(sharedPlaylist, where);
  }

  @del('/playlists/{id}/shared-playlists', {
    responses: {
      '200': {
        description: 'Playlist.SharedPlaylist DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(SharedPlaylist)) where?: Where<SharedPlaylist>,
  ): Promise<Count> {
    return await this.playlistRepository.sharedPlaylists(id).delete(where);
  }
}
