import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Music,
  AlbumMusic,
} from '../models';
import {MusicRepository} from '../repositories';

export class MusicAlbumMusicController {
  constructor(
    @repository(MusicRepository) protected musicRepository: MusicRepository,
  ) { }

  @get('/music/{id}/album-musics', {
    responses: {
      '200': {
        description: 'Array of AlbumMusic\'s belonging to Music',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': AlbumMusic } },
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<AlbumMusic>,
  ): Promise<AlbumMusic[]> {
    return await this.musicRepository.albumMusics(id).find(filter);
  }

  @post('/music/{id}/album-musics', {
    responses: {
      '200': {
        description: 'Music model instance',
        content: { 'application/json': { schema: { 'x-ts-type': AlbumMusic } } },
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Music.prototype.id,
    @requestBody() albumMusic: AlbumMusic,
  ): Promise<AlbumMusic> {
    return await this.musicRepository.albumMusics(id).create(albumMusic);
  }

  @patch('/music/{id}/album-musics', {
    responses: {
      '200': {
        description: 'Music.AlbumMusic PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody() albumMusic: Partial<AlbumMusic>,
    @param.query.object('where', getWhereSchemaFor(AlbumMusic)) where?: Where<AlbumMusic>,
  ): Promise<Count> {
    return await this.musicRepository.albumMusics(id).patch(albumMusic, where);
  }

  @del('/music/{id}/album-musics', {
    responses: {
      '200': {
        description: 'Music.AlbumMusic DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(AlbumMusic)) where?: Where<AlbumMusic>,
  ): Promise<Count> {
    return await this.musicRepository.albumMusics(id).delete(where);
  }
}
