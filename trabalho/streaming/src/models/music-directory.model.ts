import {Entity, model, property, hasMany} from '@loopback/repository';
import {Playlist} from './playlist.model';
import {SharedPlaylist} from './shared-playlist.model';

@model({settings: {}})
export class MusicDirectory extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @hasMany(() => Playlist)
  playlists: Playlist[];

  @hasMany(() => SharedPlaylist)
  sharedPlaylists: SharedPlaylist[];

  constructor(data?: Partial<MusicDirectory>) {
    super(data);
  }
}

export interface MusicDirectoryRelations {
  // describe navigational properties here
}

export type MusicDirectoryWithRelations = MusicDirectory & MusicDirectoryRelations;
