import {Entity, model, property, hasMany} from '@loopback/repository';
import {AlbumMusic} from './album-music.model';
import {PlaylistMusic} from './playlist-music.model';

@model({settings: {}})
export class Music extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  style: string;

  @property({
    type: 'number',
    required: true,
  })
  duration: number;

  @hasMany(() => AlbumMusic)
  albumMusics: AlbumMusic[];

  @hasMany(() => PlaylistMusic)
  playlistMusics: PlaylistMusic[];

  constructor(data?: Partial<Music>) {
    super(data);
  }
}

export interface MusicRelations {
  // describe navigational properties here
}

export type MusicWithRelations = Music & MusicRelations;
