import {Entity, model, property, hasMany} from '@loopback/repository';
import {PlaylistMusic} from './playlist-music.model';
import {SharedPlaylist} from './shared-playlist.model';

@model({settings: {}})
export class Playlist extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  principalStyle: string;

  @property({
    type: 'string',
    required: true,
  })
  secondaryStyle: string;

  @hasMany(() => PlaylistMusic)
  playlistMusics: PlaylistMusic[];

  @property({
    type: 'number',
  })
  musicDirectoryId?: number;

  @hasMany(() => SharedPlaylist)
  sharedPlaylists: SharedPlaylist[];

  constructor(data?: Partial<Playlist>) {
    super(data);
  }
}

export interface PlaylistRelations {
  // describe navigational properties here
}

export type PlaylistWithRelations = Playlist & PlaylistRelations;
