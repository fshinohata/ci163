import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class AlbumMusic extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  albumId?: number;

  @property({
    type: 'number',
  })
  musicId?: number;

  constructor(data?: Partial<AlbumMusic>) {
    super(data);
  }
}

export interface AlbumMusicRelations {
  // describe navigational properties here
}

export type AlbumMusicWithRelations = AlbumMusic & AlbumMusicRelations;
