import {Entity, model, property, belongsTo} from '@loopback/repository';
import {MusicDirectory} from './music-directory.model';

@model({settings: {}})
export class User extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  login: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string',
    required: true,
  })
  plan: string;

  @belongsTo(() => MusicDirectory)
  musicDirectoryId: number;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
