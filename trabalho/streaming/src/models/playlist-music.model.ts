import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class PlaylistMusic extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  playlistId?: number;

  @property({
    type: 'number',
  })
  musicId?: number;

  constructor(data?: Partial<PlaylistMusic>) {
    super(data);
  }
}

export interface PlaylistMusicRelations {
  // describe navigational properties here
}

export type PlaylistMusicWithRelations = PlaylistMusic & PlaylistMusicRelations;
