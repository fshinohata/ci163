import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class SharedPlaylist extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  musicDirectoryId?: number;

  @property({
    type: 'number',
  })
  playlistId?: number;

  constructor(data?: Partial<SharedPlaylist>) {
    super(data);
  }
}

export interface SharedPlaylistRelations {
  // describe navigational properties here
}

export type SharedPlaylistWithRelations = SharedPlaylist & SharedPlaylistRelations;
